local ok, indentbl = pcall(require, "indent_blankline")
if not ok then
	return
end

vim.opt.list = true
vim.opt.listchars:append("space:⋅")
vim.opt.listchars:append("eol:↴")

indentbl.setup({
	-- show_current_context = true,
	-- show_current_context_start = false,

	show_end_of_line = true,
	space_char_blankline = " ",
})
