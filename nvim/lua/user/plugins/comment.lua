-- import comment plugin safely
local ok, comment = pcall(require, "nvim_comment")
if not ok then
	return
end

-- enable comment
comment.setup({
	-- Linters prefer comment and line to have a space in between markers
	marker_padding = true,
	-- should comment out empty or whitespace only lines
	comment_empty = false,
	-- trim empty comment whitespace
	comment_empty_trim_whitespace = true,
	-- Should key mappings be created
	create_mappings = false,
})
