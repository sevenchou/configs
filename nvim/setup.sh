#!/bin/bash

# git
git config --global user.email "seven.zhan92@gmail.com"
git config --global user.name "sevenhi"
git config --global alias.ad add
git config --global alias.st status
git config --global alias.br branch
git config --global alias.ft fetch
git config --global alias.cm commit
git config --global alias.ck checkout
git config --global alias.pl pull
git config --global alias.ph push
git config --global alias.rt reset
git config --global alias.ch cherry-pick
git config --global alias.df diff
git config --global alias.ro remote
git config --global alias.lg "log --graph --stat"
git config --global credential.helper store

# vim
PWD=$(pwd)
DST="$HOME/.config/nvim"
[ ! -d "$DST" ] && mkdir -p "$DST"
ln -fs $PWD/init.lua $DST/init.lua
ln -fs $PWD/lua $DST/
