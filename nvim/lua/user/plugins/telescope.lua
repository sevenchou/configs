-- import telescope plugin safely
local telescope_setup, telescope = pcall(require, "telescope")
if not telescope_setup then
	return
end

local builtin_setup, builtin = pcall(require, "telescope.builtin")
if not builtin_setup then
	return
end

-- import telescope actions safely
local actions_setup, actions = pcall(require, "telescope.actions")
if not actions_setup then
	return
end

-- import telescope-ui-select safely
local themes_setup, themes = pcall(require, "telescope.themes")
if not themes_setup then
	return
end

-- configure telescope
telescope.setup({
	-- configure custom mappings
	defaults = {
		mappings = {
			i = {
				["<C-q>"] = actions.close,
				["<C-h>"] = actions.select_horizontal,
				["<C-v>"] = actions.select_vertical,
			},
		},
	},
	extensions = {
		["ui-select"] = {
			themes.get_dropdown({}),
		},
	},
})

telescope.load_extension("fzy_native")
telescope.load_extension("ui-select")
telescope.load_extension("noice")

-- mappings
local keymap = vim.keymap -- for conciseness
keymap.set("n", "<leader>sf", builtin.find_files, {}) -- find files within current working directory, respects .gitignore
keymap.set("n", "<leader>ss", builtin.live_grep, {}) -- find string in current working directory as you type
keymap.set("n", "<leader>sb", builtin.buffers, {}) -- list open buffers in current neovim instance
keymap.set("n", "<leader>sh", builtin.help_tags, {}) -- list available help tags
keymap.set("n", "<leader>gc", builtin.git_commits, {}) -- list all git commits (use <cr> to checkout) ["gc" for git commits]
keymap.set("n", "<leader>gf", builtin.git_bcommits, {}) -- list git commits for current file/buffer (use <cr> to checkout) ["gfc" for git file commits]
keymap.set("n", "<leader>gb", builtin.git_branches, {}) -- list git branches (use <cr> to checkout) ["gb" for git branch]
keymap.set("n", "<leader>gs", builtin.git_status, {}) -- list current changes per file with diff preview ["gs" for git status]
